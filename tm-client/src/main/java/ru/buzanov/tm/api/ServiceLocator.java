package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;

import java.util.Collection;

public interface ServiceLocator {

    @NotNull Collection<AbstractCommand> getCommands();

    @NotNull ITerminalService getTerminalService();

}
