package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.TerminalService;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final String adress = "http://0.0.0.0:8080/";
    @NotNull
    private final ITerminalService terminalService = new TerminalService();
    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    @Any
    @Inject
    private Instance<AbstractCommand> abstractCommands;
    @Inject
    private Thread sessionUpdateThread;
    @Inject
    private CurrentState currentState;

    @SneakyThrows
    private void init() {
        commands.clear();
        for (AbstractCommand command : abstractCommands) {
            commands.put(command.command(), command);
        }
    }

    public void start() {
        init();
        sessionUpdateThread.setDaemon(true);
        sessionUpdateThread.start();
        @NotNull String command = "";

        terminalService.printLineG("***WELCOME TO TASK MANAGER***");
        terminalService.printLine("Type help to see command list.");
        while (!commands.get("exit").command().equals(command)) {
            try {
                command = terminalService.readLine();
                if (!command.isEmpty()) {

                    if (currentState.getCurrentSession() == null && commands.get(command).isSecure())
                        terminalService.printLineR("Authorise, please");
                    else {
                        if (currentState.getCurrentUser() != null && !commands.get(command).isRoleAllow(currentState.getCurrentUser().getRoleType()))
                            terminalService.printLineR("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }
                }

            } catch (Exception | ru.buzanov.tm.endpoint.Exception e) {
                terminalService.printLineR(e.getMessage(), "  ", e.toString());
            }
        }
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}