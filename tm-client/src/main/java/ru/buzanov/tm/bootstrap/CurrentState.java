package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.User;

import javax.enterprise.context.ApplicationScoped;

@Getter
@Setter
@ApplicationScoped
public class CurrentState {
    @Nullable
    private Session currentSession;
    @Nullable
    private User currentUser;
}
