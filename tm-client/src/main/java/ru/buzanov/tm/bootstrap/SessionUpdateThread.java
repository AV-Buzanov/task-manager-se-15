package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.SessionEndpoint;

import javax.inject.Inject;

public class SessionUpdateThread extends Thread {
    @Inject
    private CurrentState currentState;
    @Inject
    private SessionEndpoint sessionEndpoint;

    @Override
    public void run() {
        while (true) {
            try {
                sleep(100000);
                if (currentState.getCurrentSession() != null) {
                    @NotNull final Session session = sessionEndpoint.updateSession(currentState.getCurrentSession());
                    currentState.setCurrentSession(session);
                }
            } catch (Exception_Exception | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
