package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Session;

public class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[PROJECT LIST]");
        @NotNull final Session session = currentState.getCurrentSession();
        for (Project project : projectService.findAllP(session)) {
            terminalService.printWBS(project);
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
