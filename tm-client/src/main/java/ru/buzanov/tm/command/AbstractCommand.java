package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;


import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.bootstrap.CurrentState;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.endpoint.Exception;

import javax.inject.Inject;

public abstract class AbstractCommand {
    @Inject
    protected ServiceLocator serviceLocator;
    @Inject
    protected ITerminalService terminalService;
    @Inject
    protected UserEndpoint userEndpoint;
    @Inject
    protected ProjectEndpoint projectService;
    @Inject
    protected TaskEndpoint taskService;
    @Inject
    protected SessionEndpoint sessionService;
    @Inject
    protected AdminUserEndpoint adminUserEndpoint;
    @Inject
    protected CurrentState currentState;

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception, java.lang.Exception;

    public abstract boolean isSecure() throws Exception, java.lang.Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }
}