package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Session;

public class ProjectEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO EDIT]");
        @NotNull final Session session = currentState.getCurrentSession();
        terminalService.printLine(projectService.getListP(session));
        @Nullable String stringBuf = projectService.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        if (stringBuf == null) throw new Exception("Wrong index.");
        @Nullable final Project project = projectService.findOneP(session, stringBuf);
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()&&projectService.isNameExistP(currentState.getCurrentSession(), stringBuf)) {
            terminalService.printLineR("This name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
        project.setName(stringBuf);
        terminalService.readWBS(project);
        projectService.mergeP(currentState.getCurrentSession(), project.getId(), project);
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
