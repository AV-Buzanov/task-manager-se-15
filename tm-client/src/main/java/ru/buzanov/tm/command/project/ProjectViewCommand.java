package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Session;
import ru.buzanov.tm.endpoint.Task;

public class ProjectViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View project information";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[CHOOSE PROJECT TO VIEW]");
        @NotNull final Session session = currentState.getCurrentSession();
        terminalService.printLine(projectService.getListP(session));
        @Nullable String idBuf = projectService.getIdByCountP(session, Integer.parseInt(terminalService.readLine()));
        @NotNull final Project project = projectService.findOneP(session, idBuf);
        terminalService.printWBSLine(project);
        terminalService.printLineG("[TASKS(", String.valueOf(taskService.findByProjectIdT(session, idBuf).size()), ")]");
        if (taskService.findByProjectIdT(session, idBuf).isEmpty()) {
            terminalService.printLine(FormatConst.EMPTY_FIELD);
            return;
        }
        for (@NotNull final Task task : taskService.findByProjectIdT(session, idBuf)) {
            terminalService.printLine(task.getName(), " : ", task.getStatus().value());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
