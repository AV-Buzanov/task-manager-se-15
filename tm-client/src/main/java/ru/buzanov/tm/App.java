package ru.buzanov.tm;

import ru.buzanov.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

/**
 * Task Manager
 */

public final class App {
    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(App.class).initialize()
                .select(Bootstrap.class).get().start();
    }
}