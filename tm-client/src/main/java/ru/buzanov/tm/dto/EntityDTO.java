package ru.buzanov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.bootstrap.CurrentState;
import ru.buzanov.tm.endpoint.*;

import javax.inject.Inject;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Project> projects;
    private List<Task> tasks;
    @Inject
    private ProjectEndpoint projectEndpoint;
    @Inject
    private TaskEndpoint taskEndpoint;
    @Inject
    private CurrentState currentState;


    public void load(@Nullable final ServiceLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        this.projects = new ArrayList<>(projectEndpoint.findAllP(currentState.getCurrentSession()));
        this.tasks = new ArrayList<>(taskEndpoint.findAllT(currentState.getCurrentSession()));
    }

    public void unLoad(@Nullable final ServiceLocator serviceLocator) throws Exception_Exception {
        if (serviceLocator == null) return;
        projectEndpoint.loadListP(currentState.getCurrentSession(), projects);
        taskEndpoint.loadListT(currentState.getCurrentSession(), tasks);
    }
}
