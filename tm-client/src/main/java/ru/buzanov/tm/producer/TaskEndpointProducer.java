package ru.buzanov.tm.producer;

import ru.buzanov.tm.endpoint.*;

import javax.enterprise.inject.Produces;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskEndpointProducer {
    @Produces
    public TaskEndpoint getTaskEndpoint() throws MalformedURLException {
        URL urlTask = new URL("http://localhost:8080/TaskEndpoint?wsdl");
        return new TaskEndpointService(urlTask).getTaskEndpointPort();
    }
}
