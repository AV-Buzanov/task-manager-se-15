package ru.buzanov.tm.producer;

import ru.buzanov.tm.endpoint.ProjectEndpoint;
import ru.buzanov.tm.endpoint.ProjectEndpointService;

import javax.enterprise.inject.Produces;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectEndpointProducer {
    @Produces
    public ProjectEndpoint getProjectEndpoint() throws MalformedURLException {
        URL urlProject = new URL("http://localhost:8080/ProjectEndpoint?wsdl");
        return new ProjectEndpointService(urlProject).getProjectEndpointPort();
    }
}
