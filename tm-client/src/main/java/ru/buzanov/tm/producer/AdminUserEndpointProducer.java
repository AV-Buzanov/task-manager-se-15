package ru.buzanov.tm.producer;

import ru.buzanov.tm.endpoint.AdminUserEndpoint;
import ru.buzanov.tm.endpoint.AdminUserEndpointService;

import javax.enterprise.inject.Produces;
import java.net.MalformedURLException;
import java.net.URL;

public class AdminUserEndpointProducer {
    @Produces
    public AdminUserEndpoint getAdminUserEndpoint() throws MalformedURLException {
        URL urlAdminUser = new URL("http://localhost:8080/AdminUserEndpoint?wsdl");
        return new AdminUserEndpointService(urlAdminUser).getAdminUserEndpointPort();
    }
}
