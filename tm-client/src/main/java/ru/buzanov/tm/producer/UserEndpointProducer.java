package ru.buzanov.tm.producer;

import ru.buzanov.tm.endpoint.UserEndpoint;
import ru.buzanov.tm.endpoint.UserEndpointService;

import javax.enterprise.inject.Produces;
import java.net.MalformedURLException;
import java.net.URL;

public class UserEndpointProducer {
    @Produces
    public UserEndpoint getUserEndpoint()  {
        URL urlUser = null;
        try {
            urlUser = new URL("http://localhost:8080/UserEndpoint?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new UserEndpointService(urlUser).getUserEndpointPort();
    }
}
