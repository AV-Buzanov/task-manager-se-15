package ru.buzanov.tm.producer;

import ru.buzanov.tm.endpoint.SessionEndpoint;
import ru.buzanov.tm.endpoint.SessionEndpointService;

import javax.enterprise.inject.Produces;
import java.net.MalformedURLException;
import java.net.URL;

public class SessionEndpointProducer {
    @Produces
    public SessionEndpoint getSessionEndpoint() throws MalformedURLException {
        URL urlSession = new URL("http://localhost:8080/SessionEndpoint?wsdl");
        return new SessionEndpointService(urlSession).getSessionEndpointPort();
    }
}
