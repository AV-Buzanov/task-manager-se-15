package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.dto.TaskDTO;

import java.util.Collection;

public interface ITaskService extends IWBSService<TaskDTO> {

    @Nullable Collection<TaskDTO> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;
}
