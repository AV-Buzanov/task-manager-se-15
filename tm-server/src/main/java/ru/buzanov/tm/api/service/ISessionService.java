package ru.buzanov.tm.api.service;

import ru.buzanov.tm.dto.SessionDTO;

public interface ISessionService extends IService<SessionDTO> {
}
