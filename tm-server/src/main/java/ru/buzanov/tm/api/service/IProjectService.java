package ru.buzanov.tm.api.service;

import ru.buzanov.tm.dto.ProjectDTO;

public interface IProjectService extends IWBSService<ProjectDTO> {

}
