package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IRepository<T> {
    @NotNull Collection<T> findAll() throws Exception;

    @Nullable T findOne(@NotNull final String id) throws Exception;
    void merge(@Nullable T user)throws Exception ;
    void remove(@NotNull final String id)throws Exception ;

    void removeAll() throws Exception;

     void load(@NotNull final T entity) throws Exception;

    void load(final List<T> list) throws Exception;
}
