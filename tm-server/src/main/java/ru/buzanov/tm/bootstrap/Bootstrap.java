package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.PropertyService;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor

public final class Bootstrap{
    @Inject
    private ProjectEndpoint projectEndpoint;
    @Inject
    private TaskEndpoint taskEndpoint;
    @Inject
    private SessionEndpoint sessionEndpoint;
    @Inject
    private AdminUserEndpoint adminUserEndpoint;
    @Inject
    private UserEndpoint userEndpoint;
    @Inject
    private PropertyService propertyService;

    @Inject
    private Thread sessionCleanThread;

    @NotNull
    private final String adress = "http://0.0.0.0:8080/";

    public void start() {
        propertyService.init();
        sessionCleanThread.setDaemon(true);
        sessionCleanThread.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Endpoint.publish(adress + "ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish(adress + "TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish(adress + "AdminUserEndpoint?wsdl", adminUserEndpoint);
        Endpoint.publish(adress + "SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish(adress + "UserEndpoint?wsdl", userEndpoint);
        System.out.println("TaskDTO manager server is running.");
        System.out.println(adress + "ProjectEndpoint?wsdl");
        System.out.println(adress + "TaskEndpoint?wsdl");
        System.out.println(adress + "AdminUserEndpoint?wsdl");
        System.out.println(adress + "SessionEndpoint?wsdl");
        System.out.println(adress + "UserEndpoint?wsdl");

        while (true) {
            try {
                if ("exit".equals(reader.readLine())) {
                    System.exit(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}