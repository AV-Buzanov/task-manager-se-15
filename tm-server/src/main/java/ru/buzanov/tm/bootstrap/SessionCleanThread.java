package ru.buzanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.util.PropertyService;

import javax.inject.Inject;

public class SessionCleanThread extends Thread {

    @Inject
    private SessionService sessionService;
    @Inject
    private PropertyService propertyService;

//    public SessionCleanThread(@NotNull final ServiceLocator entityManager) {
//        this.entityManager = entityManager;
//    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(propertyService.getSessionLifetime() * 2);
                for (SessionDTO session : sessionService.findAll()) {
                    long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
                    if (timeDif > propertyService.getSessionLifetime())
                        sessionService.remove(session.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
