package ru.buzanov.tm.bootstrap;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.util.PropertyService;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class CustomEntityManagerProducer {
    @Inject
    private PropertyService propertyService;
//    private EntityManagerFactory managerFactory = getManagerFactory();
    @Produces
    public EntityManagerFactory getManagerFactory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl2());
        settings.put(Environment.USER, propertyService.getJdbcUsername());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

//    public EntityManagerFactory getManagerFactory() {
//        return managerFactory;
//    }
//
//    public void setManagerFactory(EntityManagerFactory managerFactory) {
//        this.managerFactory = managerFactory;
//    }
}
