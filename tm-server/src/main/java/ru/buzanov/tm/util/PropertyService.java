package ru.buzanov.tm.util;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.util.Properties;

@Getter
@Setter
public class PropertyService {
    @NotNull
    private final Properties jdbc = new Properties();
    @NotNull
    private final Properties session = new Properties();

    public void init(){
//        try {
//            jdbc.load(PropertyService.class.getResourceAsStream("/jdbc.properties"));
//            session.load(PropertyService.class.getResourceAsStream("/session.properties"));
//            System.out.println(session.getProperty("lifeTime"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public String getJdbcUsername() {
        return "root";
    }

    public String getJdbcPassword() {
        return "root";
    }

    public String getJdbcDriver() {
        return "com.mysql.jdbc.Driver";
    }

    public String getJdbcUrl() {
        return "jdbcAdress";
    }

    public String getJdbcUrl2() {
        return "jdbc:mysql://localhost:3306/test";
    }

    public int getSessionLifetime() {
        return Integer.parseInt("180000");
    }

    public int getSignCycle() {
        return Integer.parseInt("123");
    }

    public String getSignSalt() {
        return "my_salt";
    }

}
