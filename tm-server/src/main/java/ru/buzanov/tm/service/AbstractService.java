package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.IService;
import ru.buzanov.tm.bootstrap.CustomEntityManagerProducer;
import ru.buzanov.tm.dto.AbstractDto;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<T extends AbstractDto> implements IService<T> {
    @Inject
    protected EntityManagerFactory entityManager;

//    public AbstractService(@NotNull ServiceLocator entityManager) {
//        this.entityManager = entityManager;
//    }

    public abstract void load(@Nullable final T entity) throws Exception;

    @NotNull
    public abstract Collection<T> findAll() throws Exception;

    public abstract void load(@Nullable final List<T> list) throws Exception;

    @Nullable
    public abstract T findOne(@Nullable final String id) throws Exception;

    public abstract void merge(@Nullable final String id, @Nullable final T entity) throws Exception;

    public abstract void remove(@Nullable final String id) throws Exception;

    public abstract void removeAll() throws Exception;
}
