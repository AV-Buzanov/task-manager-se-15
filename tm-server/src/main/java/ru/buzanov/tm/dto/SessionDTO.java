package ru.buzanov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractDto {
    @Nullable
    private String userId;
    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());
    @Nullable
    private String signature;
}
