package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.util.PropertyService;
import ru.buzanov.tm.util.SignatureUtil;

import javax.inject.Inject;

public abstract class AbstractEndpoint {
    @Inject
     IProjectService projectService;
    @Inject
     ITaskService taskService;
    @Inject
     IUserService userService;
    @Inject
     ISessionService sessionService;
    @Inject
     PropertyService propertyService;

//    public AbstractEndpoint(@NotNull final ServiceLocator entityManager) {
//        this.entityManager = entityManager;
//        this.projectService = entityManager.getProjectService();
//        this.taskService = entityManager.getTaskService();
//        this.userService = entityManager.getUserService();
//        this.sessionService = entityManager.getSessionService();
//        this.propertyService = entityManager.getPropertyService();
//    }

    protected void auth(@Nullable final SessionDTO session) throws Exception {
        if (session == null) {
            throw new Exception("null session");
        }
        long timeDif = System.currentTimeMillis() - session.getCreateDate().getTime();
        if (timeDif > propertyService.getSessionLifetime()) {
            sessionService.remove(session.getId());
            throw new Exception("SessionDTO time out.");
        }
        @Nullable final String signature = session.getSignature();
        session.setSignature(null);
        @Nullable final String ourSignature = SignatureUtil.sign(session,
                propertyService.getSignSalt(),
                propertyService.getSignCycle());
        if (signature == null || !signature.equals(ourSignature)) {
            throw new Exception("Invalid session signature.");
        }
        SessionDTO ourSession;
        if ((ourSession = sessionService.findOne(session.getId())) == null){
            throw new Exception("SessionDTO not found.");}
        if (!ourSession.getSignature().equals(ourSignature))
            throw new Exception("SessionDTO signature fail.");

        System.out.print(userService.findOne(session.getUserId()).getLogin() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getClassName() + "  ");
        System.out.print(Thread.currentThread().getStackTrace()[2].getMethodName());
        System.out.println();
    }
}
